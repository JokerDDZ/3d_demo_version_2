﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fastMovement : MonoBehaviour
{
    public float rotSpeed = 5.0f;
    public float maxSpeed = 5f;

    GameObject player;

    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        if (player == null)
        {
            Debug.Log("No kurwa nie udalo sie :(");
        }
        rb = this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        Vector3 velocity = new Vector3(maxSpeed * Time.deltaTime, 0,0 );
        pos += transform.rotation * velocity;

        transform.position = pos;

        Vector3 dir = player.transform.position - transform.position;
        dir.Normalize();


        float yAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg - 90;


        Quaternion desireRot = Quaternion.Euler(0, yAngle, 0);

       transform.rotation =  Quaternion.RotateTowards(transform.rotation, desireRot, rotSpeed * Time.deltaTime);
    }
}
